const express = require('express');
// const bodyParser = require('body-parser');
const router = express.Router();
const mongoose = require('mongoose');
const Post = mongoose.model('posts');

router.get('/', (req, res)=>{
	Post
	.find()
	.sort({date: 'descending'})
	.populate('user')
	.populate('comments.commentUser')
	.then(post => {
		// console.log(post)
		res.render('dashboard', {
			post: post
		});	
	})
   
})

router.get('/verify', (req, res) =>{
	if(req.user){
		console.log(req.user);
	} else{
		console.log('Not Auth')
	}
})

router.post('/found', (req, res)=>{
	let {itemName, itemBody} = req.body;

	var onePost = {
		itemName,
		itemBody
	}

	new Post(onePost)
	.save()
	.then((post)=>{
		res.redirect('/dashboard');
		// console.log(post);
	})

})

router.post('/comment/:id', (req, res)=>{
	let {commentBody} = req.body; 
	Post.findOne({
		_id: req.params.id
	})
	.then(post =>{

		const newComment = {
			commentBody,
			commentUser: req.user.id	
		}

		// res.send(newComment.commentBody)

		// // Adding to comments array
		post.comments.unshift(newComment);
		
		// console.log(post.comments)
		post.save()
			.then(post =>{
				res.redirect('/dashboard');
			})	
	} )

	
})

router.get('/logout', (req, res)=>{
	req.logout();
	res.redirect('/');
})

router.post('/comment/:id', (req, res) => {
  Post.findOne({
    _id: req.params.id
  })
  .then(post => {
    const newComment = {
      commentBody: req.body.commentBody,
      commentUser: req.user.id
    }

    // Add to comments array
    post.comments.unshift(newComment);

    post.save()
      .then(story => {
        res.redirect(`/posts/show/${story.id}`);
      });
  });
});

router.get('/myposts/:id', (req, res)=>{
	Post
	.findOne({
		_id : req.params.id
	})
	.then((post)=>{
		res.send('hello')
	})
})

router.get('/viewpost/:id', (req, res)=>{
	Post
	.findOne({
		_id: req.params.id
	})
	.then((post)=>{
		// console.log(post);
		res.render('postview/', {
			post: post
		})	
	})
	
})

module.exports = router;