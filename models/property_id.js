const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const propertySchema = new Schema({
	plot_no: {
		type: String,
		default: 'Null'
	},

	plan_no: {
		type: String,
		default: 'Null'
	},

	id_no: {
		type: String,
		default: 'Null'
	},

	ref_no:{
		type: String,
		default: 'Null'
	}
})

mongoose.model('properties', propertySchema)