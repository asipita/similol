const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const userSchema = new Schema({
	user_name:{
		type: String,
		required: true
	},
	password_1: {
		type: String
	}
})

mongoose.model('user', userSchema)
