const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const ownerSchema = new Schema({
	plot_no:{
		type: Number,
		required: true
	},

	occupant_name: {
		// required: true,
		type: String,
		default: 'Null'
	},

	occupancy: {
		type: String,
		default: 'Null'
		// required: true
	},

	gender: {
		type: String,
		default: 'Null'
	},

	age:{
		type: String,
		default: 'Null'
	},

	marital_status:{
		type: String,
		default: 'Null'
	},

	employment_status:{
		type: String,
		default: 'Null'
	},

	education:{
		type: String,
		default: 'Null'
	},

	qualification:{
		type: String,
		default: 'Null'
	},

	work_experience:{
		type: String,
		default: 'Null'
	}
})

mongoose.model('owners', ownerSchema)