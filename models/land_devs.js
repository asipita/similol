const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const landDevSchema = new Schema({
	plot_no:{
		type: Number,
		required: true
	},

	land_dev: {
		type: String,
		default: 'Null'
	},

	land_type: {
		type: String,
		default: 'Null'
	}
})

mongoose.model('landdev', landDevSchema)