const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const titleSchema = new Schema({
	plot_no:{
		type: Number,
		required: true
	},

	survey_plan: {
		type: String,
		default: 'Null'
	},

	cofo: {
		type: String,
		default: 'Null'
	},

	doreg: {
		type: String,
		default: 'Null'
	}
})

mongoose.model('title', titleSchema)