const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const paymentSchema = new Schema({
	plot_no:{
		type: Number,
		required: true
	},

	monthly_rent: {
		type: String,
		default: 'Null'
	},

	yearly_rent: {
		type: String,
		default: 'Null'
	}
})

mongoose.model('payment', paymentSchema)