const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const personSchema = new Schema({
	plot_no:{
		type: String,
		required: true
	},

	user_pass:{
		type: String,
		required: true
	},

	land_dev: {
		type: String,
		default: 'Null'
	},

	land_type: {
		type: String,
		default: 'Null'
	},

	occupant_name: {
		// required: true,
		type: String,
		default: 'Null'
	},

	occupancy: {
		type: String,
		default: 'Null'
	},

	gender: {
		type: String,
		default: 'Null'
	},

	age:{
		type: String,
		default: 'Null'
	},

	marital_status:{
		type: String,
		default: 'Null'
	},

	employment_status:{
		type: String,
		default: 'Null'
	},

	education:{
		type: String,
		default: 'Null'
	},

	qualification:{
		type: String,
		default: 'Null'
	},

	work_experience:{
		type: String,
		default: 'Null'
	},

	land_use: {
		type: String,
		default: 'Null'
	},

	monthly_rent: {
		type: String,
		default: 'Null'
	},

	yearly_rent: {
		type: String,
		default: 'Null'
	},

	plan_no: {
		type: String,
		default: 'Null'
	},

	id_no: {
		type: String,
		default: 'Null'
	},

	ref_no:{
		type: String,
		default: 'Null'
	},

	survey_plan: {
		type: String,
		default: 'Null'
	},

	cofo: {
		type: String,
		default: 'Null'
	},

	doreg: {
		type: String,
		default: 'Null'
	}

})

mongoose.model('person', personSchema)
