const mongoose = require('mongoose');

const Schema= mongoose.Schema;

const landSchema = new Schema({
	plot_no:{
		type: Number,
		required: true
	},

	land_use: {
		type: String,
		default: 'Null'
	}
})

mongoose.model('lands', landSchema)