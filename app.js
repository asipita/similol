const express = require('express');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// create global app

var app = express();

// load models

require('./models/land_ownership')
require('./models/property_id')
require('./models/land_use')
require('./models/land_devs')
require('./models/payments')
require('./models/titles')
require('./models/user')
require('./models/person')


const landOwnership = mongoose.model('owners')
const propertyID = mongoose.model('properties')
const landUse = mongoose.model('lands')
const landDevs = mongoose.model('landdev')
const payments = mongoose.model('payment')
const titles = mongoose.model('title')
const user = mongoose.model('user')
const persona = mongoose.model('person')

// configuring mongoose

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/simi-dev', {
// mongoose.connect(keys.mongoURI, {
        useMongoClient: true
})
  .then(() => console.log('connected to database'))
  .catch(err => console.error(err))


// configuring handlebars

app.engine('handlebars', exphbs({
	defaultLayout: 'main'
}))

app.set('view engine', 'handlebars');

// setting static folder

app.use(express.static(__dirname + '/public'));

// body-parser middleware

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use routes

app.get('/', (req, res)=>{
	res.render('index')
})

app.post('/login', (req, res)=>{
  let {user_name, password_1} = req.body;

  persona
  .findOne({
    plot_no: user_name,
    user_pass: password_1
  })
  .then((data)=>{
    if(data !== null){
      res.render('dashboard', {
        data: data
      })
    }
    console.log(data);
  })

  // user
  // .findOne({
  //   user_name,
  //   password_1
  // })
  // .then(data=>{
  //   if(data !== null){
  //     person
  //     .findOne({
  //         plot_no: user_name
  //     })
  //     .then((snapshot)=>{
  //       console.log(snapshot);
  //       res.render('dashboard', {
  //         snapshot: snapshot
  //       })
  //     })
  //   } else{
  //     res.render('index', {
  //       msg: `${user_name} is not a registered user, pls try again`
  //     })
  //   }
  // })
});

app.get('/options', (req, res)=>{
	res.render('options');
})


app.get('/admin', (req, res)=>{
	res.render('admin');
})

app.post('/addNewUser', (req, res)=>{
  let {user_name, password_1, password_2} = req.body;

  if(password_1 !== password_2){
    res.render('form', {
      msg: 'Passwords do not match'
    })
  } else{
    new user({
      user_name,
      password_1
    })
    .save()
    .then(data=>{
      console.log(data);
      res.render('form', {
        msg: `${data.user_name} was added as a user`
      })
    })
  }
})

app.post('/adminlogin', (req, res)=>{
  let {usern, passw} = req.body;

  if(usern === 'Simi' && passw === 'simisola'){
      res.render('form')
  } else{

    res.render('admin', {
      err: 'you are not admin, try again'
    })
  }


})

app.post('/uploadForm', (req, res)=>{
	console.log('starting...');

	let {plot_number, user_pass, occupant_name, occupancy, gender, age, marital_status, employment_status, education, qualification, work_experience, plot_no, plan_no, id_no, ref_no, land_use, land_dev, land_type, monthly_rent, yearly_rent, survey_plan, cofo, doreg } = req.body;

  new persona({
    plot_no,
    user_pass,
    occupant_name,
    occupancy,
    gender,
    age,
    marital_status,
    employment_status,
    education,
    qualification,
    work_experience,
    plan_no,
    id_no,
    ref_no,
    land_use,
    land_dev,
    land_type,
    monthly_rent,
    yearly_rent,
    survey_plan,
    cofo,
    doreg
  })
  .save()
  .then((data)=>{
    console.log('person saved')
  })

	new landOwnership({
		plot_no,
		occupant_name,
		occupancy,
		gender,
		age,
		marital_status,
		employment_status,
		education,
		qualification,
		work_experience
	})
	.save()
	.then((data)=>{
		console.log(`${data.occupant_name}'s document was uploaded`)
	})

	new propertyID({
		plot_no,
		plan_no,
		id_no,
		ref_no
	})
	.save()
	.then((data)=>{
		console.log('second batch saved')
	})

	new landUse({
		plot_no,
		land_use
	})
	.save()
	.then((data)=>{
		console.log('third batch saved')
	})

	new landDevs({
		plot_no,
		land_dev,
		land_type
	})
	.save()
	.then((data)=>{
		console.log('fourth batch saved')
	})

	new payments({
		plot_no,
		monthly_rent,
		yearly_rent
	})
	.save()
	.then((data)=>{
		console.log('fifth batch saved')
	})

	new titles({
		plot_no,
		survey_plan,
		cofo,
		doreg
	})
	.save()
	.then((data)=>{
		console.log('sixth batch saved')
	})

	res.render('form');
})

app.get('/landOwnership', (req, res)=>{
	landOwnership
	.find()
	.then((data)=>{
		// console.log(data);
		res.render('ownership_page',{
			data: data
		})
	})
})

app.get('/propertyid', (req, res)=>{
	propertyID
	.find()
	.then((data)=>{
		res.render('property_page',{
			data: data
		})
	})
})

app.get('/landUse', (req, res) => {
	landUse
	.find()
	.then((data)=>{
		console.log(data);
		res.render('landuse_page', {
			data: data
		})
	})
})

app.get('/paymentRange',(req, res) => {
  payments
  .find()
  .then((data)=>{
    res.render('payment_page', {
      data: data
    })
  })
})

app.get('/title',(req, res) => {
  titles
  .find()
  .then((data)=>{
    res.render('titles_page', {
      data: data
    })
  })
})

app.get('/landDev', (req, res) => {
  landDevs
  .find()
  .then((data) => {
    res.render('landev', {
      data: data
    })
  })
})

const port = process.env.PORT || 4040

app.listen(port, () => {
    console.log(`app running on port ${port}`)
})
